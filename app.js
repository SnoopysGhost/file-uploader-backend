const express = require("express");
const bodyParser = require("body-parser");
const multer = require("multer");
const path = require("path");
//Routes
const uploadFiles = require("./routes/fileUploads");

const app = express();

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads");
  },
  filename: (req, file, cb) => {
    // just adding the date in front of the file name to make is more unique
    // and preventing files to be overwritten
    cb(null, new Date().toISOString() + "_" + file.originalname);
  },
});

// middleware
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, PATCH, DELETE"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

app.use(bodyParser.json());
app.use(multer({ storage: fileStorage }).array("file"));
app.use(express.static(path.join(__dirname, "public")));
app.use(uploadFiles);

app.listen(8080);
