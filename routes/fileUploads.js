const express = require("express");
const fileUploadController = require("../controllers/fileUploads");
const router = express.Router();

router.post("/fileUploads", fileUploadController.fileUploads);

module.exports = router;
